#FROM openjdk:slim
#COPY --from=python:3.6-slim / /

FROM ubuntu:18.04

ENV COVERITY_VERSION=2021.12.1
ENV COVERITY_LICENSE ""

RUN apt-get update \
  && apt-get install -y curl libxml2-dev libxslt-dev libcurl4-openssl-dev libreadline6-dev libssl-dev patch build-essential zlib1g-dev openssh-server libyaml-dev libicu-dev \
  && apt-get install -y openjdk-8-jdk python3 python3-pip \
  && apt-get install -y curl unzip git jq sudo \
  && apt-get install -y ruby \
  && apt-get install -y maven \
  && gem install bundler \
  && useradd -m runner \
  && usermod -aG sudo runner \
  && echo "%sudo ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

#RUN python3 -m ensurepip
RUN pip3 install --upgrade pip && pip3 install requests==2.26.0 urllib3==1.26.7 jsonapi-requests==0.6.2 tenacity==6.2.0 python-gitlab suds

RUN curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash - && apt -y install nodejs

# Install the runner
RUN curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
RUN sudo apt-get install gitlab-runner

COPY --chown=gitlab-runner:gitlab-runner entrypoint.sh run.sh /home/gitlab-runner
RUN sudo chmod u+x /home/gitlab-runner/entrypoint.sh /home/gitlab-runner/run.sh

COPY cov-analysis-linux64-${COVERITY_VERSION}.tar.gz ./
RUN tar xzf cov-analysis-linux64-${COVERITY_VERSION}.tar.gz && rm cov-analysis-linux64-${COVERITY_VERSION}.tar.gz && mv cov-analysis-linux64-${COVERITY_VERSION} cov-analysis-linux64

COPY dot-profile /root/.profile

ENTRYPOINT ["/home/gitlab-runner/entrypoint.sh"]
