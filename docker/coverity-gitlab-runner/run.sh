#!/bin/bash

trap 'kill -INT $PID' TERM INT

if [ -f ".path" ]; then
    export PATH=`cat .path`
    echo ".path=${PATH}"
fi

# Insert Sunopsys PATH variables here
export PATH=~/cov-analysis-linux64/bin:$PATH

gitlab-runner run

#PID=$!
#wait $PID
#trap - TERM INT
#wait $PID
