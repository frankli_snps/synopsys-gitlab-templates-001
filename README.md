# Synopsys GitLab CI Templates

Modern applications are a complex mix of proprietary and open source code, APIs and user interfaces, application behavior, and deployment workflows. Security issues at any point in this software supply chain can leave you and your customers  at risk. Synopsys solutions help you identify and   manage software supply chain risks end-to-end.

The Synopsys GitLab CI Templates repository contains a collection of .yml templates
that can be used to integrate Synopsys AST solutions into your GitLab CI pipelines.

You may reference the templates directly from this repository, **but it is recommended that you clone or fork this
repo** so that you are running a known "good" version in your production DevOps pipelines and can modify the templates
to suit your specific needs.

These templates and scripts are provided under an OSS license (specified in the LICENSE file) and has been developed by Synopsys field engineers as a contribution to the Synopsys user community. Please direct questions and comments to the appropriate forum in the Synopsys user community.

# Available Templates

## Coverity

Run a Coverity SAST scan as part of your GitLab CI pipeline.
These templates are written for the traditional cov-build or cov-capture workflow, with cov-analyze or
cov-run-desktop running locally and a central Coverity Connect server. There are two instances of this recipe:

- [coverity](templates/README-coverity.md) - For traditional Coverity, using Coverity Connect and 
cov-build/cov-capture/cov-analyze. This template can be added to your pipeline to add a complete Coverity
workflow. This recipe implements the current recommended best practices for running Coverity in a GitLab CI
pipeline and the README can provide more details.
- [coverity-thin-client](templates/README-coverity-thin-client.md) - Coming soon! A template that uses the
new Coverity CLI and Scan Service to offload analysis jobs from your GitLab runners.

# Future Enhancements

Future templates will include:

- Coverity CLI support, including the new Thin Client and Scan Service
- Polaris support
